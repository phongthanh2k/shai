﻿using SHAI.Helpers;
using FluentValidation;
using System.ComponentModel.DataAnnotations;
using SHAI.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace SHAI.ViewModels
{
    public class UserViewModel : UserBaseViewModel
    {
        public UserViewModel() : base() { }

        public UserViewModel(ApplicationUser u, string[] roles, ApplicationDbContext _context)
        {
            var _group = _context.Groups.FromSqlRaw(@"SELECT Groups.*
                                            FROM dbo.UserGroups
                                            INNER JOIN dbo.Groups ON UserGroups.GroupsId = Groups.Id
                                            WHERE UserGroups.UsersId = {0}", u.Id).Select(g => g.Name).ToArray();
            Orders = _context.Orders.FromSqlRaw(@"SELECT *
                                            FROM dbo.Orders
                                            WHERE Orders.MemberId = {0}", u.Id).Select(o => new OrderViewModel(o, _context)).ToArray();
            Id = u.Id;
            UserName = u.UserName;
            FullName = u.FullName;
            ChineseName = u.ChineseName;
            Email = u.Email;
            PhoneNumber = u.PhoneNumber;
            Address = u.Address;
            BirthDate = u.BirthDate;
            JoinDate = u.JoinDate;
            Gender = u.Gender;
            IsEnabled = u.IsEnabled;
            IsLockedOut = u.IsLockedOut;
            JobTitle = u.JobTitle;
            Roles = roles;
            RoleString = String.Join("|", roles);
            Configuration = u.Configuration;
            IsStaff = u.IsStaff;
            Groups = _group;
            GroupString = String.Join("|", _group);
            MemberShip = u.MemberShip;
            MemberShipStartDate = u.MemberShipStartDate;
            ExpirationDate = u.ExpirationDate;
            //MemberShip = u.MembershipFee?.MemberId;
        }

        public bool IsLockedOut { get; set; }

        [MinimumCount(1, ErrorMessage = "Roles cannot be empty")]
        public string[] Roles { get; set; }

        public string RoleString { get; set; }

        public string[] Groups { get; set; }

        public string GroupString { get; set; }

        public ICollection<OrderViewModel> Orders { get; set; } 
    }

    public class UserEditViewModel : UserBaseViewModel
    {
        public string CurrentPassword { get; set; }

        [MinLength(6, ErrorMessage = "New Password must be at least 6 characters")]
        public string NewPassword { get; set; }

        [MinimumCount(1, ErrorMessage = "Roles cannot be empty")]
        public string[] Roles { get; set; }
    }

    public class UserPatchViewModel
    {
        public string FullName { get; set; }

        public string JobTitle { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime BirthDate { get; set; }

        public string Gender { get; set; }

        public string Configuration { get; set; }
    }

    public class UserGroupViewModel : UserBaseViewModel
    {
        public UserGroupViewModel() : base() { }

        public UserGroupViewModel(ApplicationUser u)
        {
            Id = u.Id;
            UserName = u.UserName;
            FullName = u.FullName;
            ChineseName = u.ChineseName;
            Email = u.Email;
            PhoneNumber = u.PhoneNumber;
            BirthDate = u.BirthDate;
            JoinDate = u.JoinDate;
            Gender = u.Gender;
            IsEnabled = u.IsEnabled;
            JobTitle = u.JobTitle;
            Configuration = u.Configuration;
            IsStaff = u.IsStaff;

            MemberShip = u.MemberShip;
            MemberShipStartDate = u.MemberShipStartDate;
            ExpirationDate = u.ExpirationDate;
        }
    }

    public abstract class UserBaseViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Username is required"), StringLength(200, MinimumLength = 2, ErrorMessage = "Username must be between 2 and 200 characters")]
        public string UserName { get; set; }

        public string FullName { get; set; }
        public string ChineseName { get; set; }

        [Required(ErrorMessage = "Email is required"), StringLength(200, ErrorMessage = "Email must be at most 200 characters"), EmailAddress(ErrorMessage = "Invalid email address")]
        public string Email { get; set; }

        public string JobTitle { get; set; }

        public string PhoneNumber { get; set; }
        public string Address { get; set; }

        public DateTime BirthDate { get; set; }

        public DateTime JoinDate { get; set; }

        public string Gender { get; set; }

        public string Configuration { get; set; }

        public bool IsEnabled { get; set; }

        public bool IsStaff { get; set; }

        public int? MemberShip { set; get; }
        public DateTime? MemberShipStartDate { get; set; }
        public DateTime? ExpirationDate { get; set; }
    }

    public class UserViewModelValidator : AbstractValidator<UserViewModel>
    {
        public UserViewModelValidator()
        {
            //Validation logic here
            RuleFor(user => user.UserName).NotEmpty().WithMessage("Username cannot be empty");
            RuleFor(user => user.Email).EmailAddress().NotEmpty();
            //RuleFor(user => user.Password).NotEmpty().WithMessage("Password cannot be empty").Length(4, 20);
        }
    }
}
