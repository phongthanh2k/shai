
export class User {
    // Note: Using only optional constructor properties without backing store disables typescript's type checking for the type
    constructor(id?: string, userName?: string, fullName?: string, email?: string, birthDate?: Date, gender?: string, jobTitle?: string, phoneNumber?: string, address?: string, roles?: string[]) {

        this.id = id;
        this.userName = userName;
        this.fullName = fullName;
        this.email = email;
        this.birthDate = birthDate;
        this.gender = gender;
        this.jobTitle = jobTitle;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.roles = roles;
    }


    get friendlyName(): string | any {
        let name = this.fullName || this.userName;

        if (this.jobTitle) {
            name = this.jobTitle + ' ' + name;
        }

        return name;
    }


    public id?: string;
    public userName?: string;
    public fullName?: string;
    public email?: string;
    public birthDate?: Date;
    public gender?: string;
    public jobTitle?: string;
    public phoneNumber?: string;
    public address?: string;
    public isEnabled?: boolean;
    public isLockedOut?: boolean;
    public roles?: string[];
}
