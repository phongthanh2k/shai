import { Component } from '@angular/core';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
  animations: [fadeInOut],
  host: { 'class': 'app-not-found' }
})
export class NotFoundComponent {
}
