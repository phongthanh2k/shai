import { DefaultUrlSerializer, RouterModule, Routes, UrlSerializer, UrlTree } from '@angular/router';
import { Injectable, NgModule } from '@angular/core';

import { AuthGuard } from './services/auth-guard.service';
import { AuthService } from './services/auth.service';
import { ChangePasswordFormComponent } from './shared/components/change-password-form/change-password-form.component';
import { CreateAccountFormComponent } from './shared/components/create-account-form/create-account-form.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginFormComponent } from './shared/components/login-form/login-form.component';
import { NotFoundComponent } from './shared/components/not-found/not-found.component';
import { ResetPasswordFormComponent } from './shared/components/reset-password-form/reset-password-form.component';
import { Utilities } from './services/utilities';
import { CreateComponent } from './modules/nhanvien/create/create.component';

@Injectable()
export class LowerCaseUrlSerializer extends DefaultUrlSerializer {
  parse(url: string): UrlTree {
    const possibleSeparators = /[?;#]/;
    const indexOfSeparator = url.search(possibleSeparators);
    let processedUrl: string;

    if (indexOfSeparator > -1) {
      const separator = url.charAt(indexOfSeparator);
      const urlParts = Utilities.splitInTwo(url, separator);
      urlParts.firstPart = urlParts.firstPart.toLowerCase();

      processedUrl = urlParts.firstPart + separator + urlParts.secondPart;
    } else {
      processedUrl = url.toLowerCase();
    }

    return super.parse(processedUrl);
  }
}

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginFormComponent
  },
  {
    path: 'reset-password',
    component: ResetPasswordFormComponent,
  },
  {
    path: 'create-account',
    component: CreateAccountFormComponent
  },
  {
    path: 'change-password/:recoveryCode',
    component: ChangePasswordFormComponent,
    canActivate: [AuthGuard]
  },
  { path: 'users', loadChildren: () => import('./modules/users/users.module').then(m => m.UsersModule), canActivate: [AuthGuard], data: { title: 'User' } },
  { path: 'courses', loadChildren: () => import('./modules/courses/courses.module').then(m => m.CoursesModule), canActivate: [AuthGuard], data: { title: 'Course' } },
  { path: 'class', loadChildren: () => import('./modules/class-rooms/class-rooms.module').then(m => m.ClassRoomsModule), canActivate: [AuthGuard], data: { title: 'Class' } },
  { path: 'nhanvien', loadChildren: () => import('./modules/nhanvien/nhanvien.module').then(m => m.NhanVienModule), canActivate: [AuthGuard], data: { title: 'Nhan Vien' } },
  { path: 'nhanvien/create/:id', component: CreateComponent},
  { path: '**', component: NotFoundComponent, data: { title: 'Page Not Found' } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    AuthService,
    AuthGuard,
    { provide: UrlSerializer, useClass: LowerCaseUrlSerializer }]
})
export class AppRoutingModule { }
