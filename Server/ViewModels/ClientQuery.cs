﻿namespace SHAI.ViewModels
{
    public class ClientQuery
    {
        public ClientQuery()
        {

        }

        public int Skip { get; set; }

        public int Take { get; set; } 

        public bool RequireTotalCount { get; set; }

        public string Sort { get; set; }
    }

    public class ClientSort
    {
        public string Selector { get; set; }
        public bool Desc { get; set; }
    }
}
