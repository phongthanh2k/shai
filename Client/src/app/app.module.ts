import { ErrorHandler, NgModule, EventEmitter } from '@angular/core';
import { LoginFormComponent, ResetPasswordFormComponent } from './shared/components';


import { AccountEndpoint } from './services/account-endpoint.service';
import { AccountService } from './services/account.service';
import { AlertService } from './services/alert.service';
import { AppComponent } from './app.component';
import { AppErrorHandler } from './app-error.handler';
import { AppRoutingModule } from './app-routing.module';
import { AppTitleService } from './services/app-title.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentLoaderDirective } from './directives/component-loader.directive';
import { ConfigurationService } from './services/configuration.service';
import { CourseEndpoint } from './services/course-endpoint.service';
import { CourseService } from './services/course.service';
import { HttpClientModule } from '@angular/common/http';
import { LocalStoreManager } from './services/local-store-manager.service';
import { NotificationEndpoint } from './services/notification-endpoint.service';
import { NotificationService } from './services/notification.service';
import { OAuthModule } from 'angular-oauth2-oidc';
import { OidcHelperService } from './services/oidc-helper.service';
import { SharedModule } from './shared/shared.module';
import { SignalRService } from './services/signalr.service';
import { ThemeManager } from './services/theme-manager';
import { UnauthenticatedContentModule } from './unauthenticated-content';

@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    ResetPasswordFormComponent,

    // Directive
    ComponentLoaderDirective,

  ],
  imports: [
    AppRoutingModule,
    BrowserAnimationsModule,
    BrowserModule,
    HttpClientModule,
    OAuthModule.forRoot(),
    SharedModule,
    UnauthenticatedContentModule,
  ],
  providers: [
    { provide: ErrorHandler, useClass: AppErrorHandler },
    AccountEndpoint,
    AccountService,
    CourseEndpoint,
    CourseService,
    AlertService,
    AppTitleService,
    ConfigurationService,
    LocalStoreManager,
    NotificationEndpoint,
    NotificationService,
    OidcHelperService,
    SignalRService,
    ThemeManager,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
