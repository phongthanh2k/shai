﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SHAI.Core
{
    public static class ApplicationPermissions
    {
        public static ReadOnlyCollection<ApplicationPermission> AllPermissions;

        public const string UsersPermissionGroupName = "User Permissions";
        public static ApplicationPermission ViewUsers = new ApplicationPermission("View Users", "users.view", UsersPermissionGroupName, "Permission to view other users account details");
        public static ApplicationPermission ManageUsers = new ApplicationPermission("Manage Users", "users.manage", UsersPermissionGroupName, "Permission to create, delete and modify other users account details");

        public const string RolesPermissionGroupName = "Role Permissions";
        public static ApplicationPermission ViewRoles = new ApplicationPermission("View Roles", "roles.view", RolesPermissionGroupName, "Permission to view available roles");
        public static ApplicationPermission ManageRoles = new ApplicationPermission("Manage Roles", "roles.manage", RolesPermissionGroupName, "Permission to create, delete and modify roles");
        public static ApplicationPermission AssignRoles = new ApplicationPermission("Assign Roles", "roles.assign", RolesPermissionGroupName, "Permission to assign roles to users");

        public const string CoursesPermissionGroupName = "Course Permissions";
        public static ApplicationPermission ViewCourses = new ApplicationPermission("View Courses", "courses.view", CoursesPermissionGroupName, "Permission to view available courses");
        public static ApplicationPermission ManageCourses = new ApplicationPermission("Manage Courses", "courses.manage", CoursesPermissionGroupName, "Permission to create, delete and modify courses");

        public const string OrdersPermissionGroupName = "Order Permissions";
        public static ApplicationPermission ViewOrders = new ApplicationPermission("View Orders", "orders.view", OrdersPermissionGroupName, "Permission to view available orders");
        public static ApplicationPermission ManageOrders = new ApplicationPermission("Manage Orders", "orders.manage", OrdersPermissionGroupName, "Permission to create, delete and modify orders");

        public const string MembersPermissionGroupName = "Member Permissions";
        public static ApplicationPermission ViewMembers = new ApplicationPermission("View Members", "members.view", MembersPermissionGroupName, "Permission to view available members");
        public static ApplicationPermission ManageMembers = new ApplicationPermission("Manage Members", "members.manage", MembersPermissionGroupName, "Permission to create, delete and modify members");

        static ApplicationPermissions()
        {
            List<ApplicationPermission> allPermissions = new List<ApplicationPermission>()
            {
                ViewUsers,
                ManageUsers,

                ViewRoles,
                ManageRoles,
                AssignRoles,

                ViewCourses,
                ManageCourses,

                ViewOrders,
                ManageOrders,

                ViewMembers,
                ManageMembers
            };

            AllPermissions = allPermissions.AsReadOnly();
        }

        public static ApplicationPermission GetPermissionByName(string permissionName)
        {
            return AllPermissions.Where(p => p.Name == permissionName).SingleOrDefault();
        }

        public static ApplicationPermission GetPermissionByValue(string permissionValue)
        {
            return AllPermissions.Where(p => p.Value == permissionValue).SingleOrDefault();
        }

        public static string[] GetAllPermissionValues()
        {
            return AllPermissions.Select(p => p.Value).ToArray();
        }

        public static string[] GetAdministrativePermissionValues()
        {
            return new string[] { ManageUsers, ManageRoles, AssignRoles };
        }
    }

    public class ApplicationPermission
    {
        public ApplicationPermission()
        { }

        public ApplicationPermission(string name, string value, string groupName, string description = null)
        {
            Name = name;
            Value = value;
            GroupName = groupName;
            Description = description;
        } 


        public string Name { get; set; }
        public string Value { get; set; }
        public string GroupName { get; set; }
        public string Description { get; set; }


        public override string ToString()
        {
            return Value;
        }


        public static implicit operator string(ApplicationPermission permission)
        {
            return permission.Value;
        }
    }
}
