import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, mergeMap, tap } from 'rxjs/operators';
import { NhanVien } from '../models/nhanvien.model';
import { AuthService } from './auth.service';
import { NhanVienEnpoint } from './nhanvien-endpoint.service';


//const apiURL = 'http://localhost:5000/api/NhanVien/';

@Injectable({
        providedIn: 'root'
})
export class NhanvienService {

        private httpOptions = {
                headers: new HttpHeaders({ 'Content-Type': 'Application/json', })
        };
        private apiUrl = 'http://localhost:5000/api/NhanVien/nhanvien';
        constructor(private httpClient: HttpClient,
                private nhanvienEndpoint: NhanVienEnpoint
        ) { }

        getAllNhanVien(nhanvien?: NhanVien) {
                return this.nhanvienEndpoint.getNhanVienAllEndpoint<NhanVien>(nhanvien);
        }

        // Search(keyword: string) {
        //         const url = `${this.apiUrl}` +"?search" + keyword;
        //         return this.httpClient.get(url, this.httpOptions).pipe(catchError(this.handleError));
        // }
        getNhanVien(id: number) {
                const url = `${this.apiUrl}/create/` + id;
                return this.httpClient.get<any>(url, this.httpOptions).pipe(catchError(this.handleError));
        }

        newNhanVien(nhanvien: NhanVien) {
                const url = `${this.apiUrl}/create`;
                return this.httpClient.post<any>(url, nhanvien, this.httpOptions).pipe(catchError(this.handleError));
        }

        modifyNhanVien(id: number, nhanvien: NhanVien) {
                const url = `${this.apiUrl}/create/` + id;
                return this.httpClient.put<any>(url, nhanvien, this.httpOptions).pipe(catchError(this.handleError));
        }
        //   updateNhanVien(nhanvien: NhanVien, id?: string){
        //           const url = `${this.apiUrl}/create`+id;
        //           return this.httpClient.put(url, nhanvien, httpOptions).pipe(catchError(this.handleError));
        //   }

        public deleteNhanVien(id: number) {
                const url = `${this.apiUrl}/` + id;
                return this.httpClient.delete<any>(url).pipe(catchError(this.handleError));
        }

        private handleError(error: HttpErrorResponse) {
                if (error.error instanceof ErrorEvent) {
                        // A client-side or network error occurred. Handle it accordingly.
                        console.error('An error occurred:', error.error.message);
                } else {
                        // The backend returned an unsuccessful response code.
                        // The response body may contain clues as to what went wrong,
                        console.error(
                                `Backend returned code ${error.status}, ` + `body was: ${error.error}`
                        );
                }
                // return an observable with a user-facing error message
                return throwError('Something bad happened; please try again later.');
        }
}

