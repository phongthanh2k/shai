import { DOCUMENT } from '@angular/common';
import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  SimpleChanges,
  HostListener,
  ViewContainerRef,
  Renderer2,
  Inject,
  ViewChild,
  ComponentFactoryResolver, Injectable, Component,
} from '@angular/core';
import { ChangePasswordFormComponent, LoginFormComponent } from '../shared/components';


@Injectable({
  providedIn: 'root'
})
export class ComponentLoaderService {

  compMap: any = {
    login: LoginFormComponent,
    'change-passwor': ChangePasswordFormComponent
  };

  comps = [LoginFormComponent, ChangePasswordFormComponent]
    .map((widget: any) => [widget.id, widget])
    .reduce((widgets, [id, widget]) => Object.assign(widgets, { [id]: widget }), {});

  constructor(private resolver: ComponentFactoryResolver) {
    console.log(this.compMap);
  }

  loadComponent(name: string) {
    return this.resolver.resolveComponentFactory(this.compMap[name]);
  }
}

export interface LoaderConfig {
  componentName: string;
  action: string;
}
@Directive({
  selector: '[appComponentLoader]',
})
export class ComponentLoaderDirective {
  private _loaderActive = false;

  @ViewChild('container', { read: ViewContainerRef }) viewContainerRef!: ViewContainerRef;

  constructor(
    private componentLoader: ComponentLoaderService,
    private elementRef: ElementRef,
    private renderer: Renderer2,
    @Inject(DOCUMENT) document: Document,
    private vr: ViewContainerRef,
    private componentFactoryResolver: ComponentFactoryResolver
  ) { }

  ngOnChanges(changes: SimpleChanges) {

    // this.embedComponent();
  }
  embedComponent(name: string) {
    const componentRef = this.componentLoader.loadComponent(name);
    if (componentRef) {
      this.vr.clear();
      const ref = this.vr.createComponent(componentRef); //just appending outside somewhere
      this.renderer.appendChild(this.elementRef.nativeElement, ref.location.nativeElement)
    }
  }

  removeComponent() {
    this.vr.detach();
  }

}