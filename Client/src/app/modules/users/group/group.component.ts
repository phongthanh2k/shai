import { Component, Injector, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxFormComponent } from 'devextreme-angular';
import { clone, has, map } from 'lodash';

import { AbstractListComponent } from 'src/app/shared/components/abstract.component';
import { AccountService } from 'src/app/services/account.service';
import { Course } from 'src/app/models/course.model';
import { CourseService } from 'src/app/services/course.service';
import CustomStore from 'devextreme/data/custom_store';
import { Group } from 'src/app/models/group.models';
import { HttpParams } from '@angular/common/http';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss'],
  animations: [fadeInOut],
})
export class GroupComponent extends AbstractListComponent implements OnInit {

  @ViewChild(DxDataGridComponent, { static: false }) dataSourceGrid!: DxDataGridComponent;
  @ViewChild(DxDataGridComponent, { static: false }) memberGrid!: DxDataGridComponent;
  @ViewChild(DxFormComponent, { static: false }) taskForm!: DxFormComponent;

  members: any;
  membersSelectedRows: any = [];

  popupMemberVisible = false;

  constructor(injector: Injector, private courseService: CourseService) {
    super(injector);

    this.ayncValidate = this.ayncValidate.bind(this);
  }

  ngOnInit(): void {
    const self = this;

    this.dataSource = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();

        self.queryParams.forEach((i) => {
          if (i in loadOptions && self.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });

        return self.accountService.getGroups(params).toPromise();
      },
      remove: async (id) => {
        this.accountService.deleteGroup(id).subscribe(response => this.saveSuccessHelper(this.obj), error => this.saveFailedHelper(error));
      }
    });

    this.members = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();

        if (loadOptions.filter) {
          loadOptions.filter.push(['isStaff', '=', false]);
        } else {
          loadOptions.filter = ['isStaff', '=', false];
        }

        this.queryParams.forEach((i) => {
          if (i in loadOptions && this.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });

        return this.accountService.getAllUsers(params).toPromise();
      },
      insert: async (values) => {
        console.log(values);

        try { await this.accountService.newGroup(values).toPromise(); }
        catch (resp: any) { console.log(resp.error); }
      },
      update: async (id, params) => {
        // try { await this.http.put(`${this.apiUrl}/group/${id}`, id === 'bulkUpdate' ? params : { $set: params }).toPromise(); }
        // catch (resp) { throw resp.error; }
        try { await this.accountService.updateGroupMember(params, id).toPromise(); }
        catch (resp: any) { console.log(resp.error); }
      },
      remove: async (id) => {
        console.log(id);
        // try { await this.http.delete(`${this.apiUrl}/group/${id}`).toPromise(); }
        // catch (resp) { throw resp.error; }
      }
    });
  }

  async onSave(e: any) {
    e.preventDefault();

    if (this.taskForm.instance.validate().isValid) {
      has(this.obj, 'id') ?
        this.accountService.updateGroup(Object.assign(this.obj, { users: this.membersSelectedRows })).subscribe(response => this.saveSuccessHelper(this.obj), error => this.saveFailedHelper(error)) :
        this.accountService.newGroup(this.obj).subscribe(response => this.saveSuccessHelper(this.obj), error => this.saveFailedHelper(error));

      await this.dataSourceGrid.instance.refresh();

      this.popupVisible = false;
    }
  }

  async onAddUserToGroup(d: { key: any; }) {
    await this.members.update(this.obj.id, { member: d.key, type: 'add' });

    setTimeout(() => {
      this.membersSelectedRows.push(d.key);
      this.memberGrid.instance.refresh();
      // this.obj.users.push(this.obj);
    }, 100);

  }

  async onRemoveUserToGroup(d: { key: any; }) {

    await this.members.update(this.obj.id, { member: d.key, type: 'remove' });

    setTimeout(() => {
      this.membersSelectedRows = this.membersSelectedRows.filter((item: any) => item !== d.key);
      this.memberGrid.instance.refresh();

      // this.obj.users = this.obj.users.filter(item => item._id !== this.obj._id);
    }, 100);
  }

  async onCloseMemberPopup() {
    this.popupVisible = false;
  }

  async onMemberEdit(cell: any = null) {
    this.popupMemberVisible = !this.popupMemberVisible;
    this.obj = {};
    if (this.taskForm) this.taskForm.instance.resetValues();

    if (this.taskForm && (cell && cell.row && cell.row.data)) this.taskForm.formData = this.obj = clone(cell.row.data);

    this.membersSelectedRows = (cell && cell.row && cell.row.data) ? map(cell.row?.data.users, r => r.id) : [];
  }

  private saveSuccessHelper(group?: Group) {

    this.popupVisible = false;
    this.dataSourceGrid.instance.refresh();

    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }
}
