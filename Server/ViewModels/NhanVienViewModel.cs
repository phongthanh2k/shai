﻿using FluentValidation;
using SHAI.DAL.Models;
using SHAI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHAI.ViewModels
{
    public class NhanVienBaseViewModel : AuditableEntity
    {
        public int Id { get; set; }
        public string HoTen { get; set; }
        public string GioiTinh { get; set; }
        public string SoThich { get; set; }
        public string HocLuc { get; set; }
        public string Sdt { get; set; }
    }

    public class NhanVienViewModel : NhanVienBaseViewModel
    {
        public NhanVienViewModel() : base() { }

        public NhanVienViewModel(NhanVien o)
        {
            Id = o.Id;
            HoTen = o.HoTen;
            GioiTinh = o.GioiTinh;
            SoThich = o.SoThich;
            HocLuc = o.HocLuc;
            Sdt = o.Sdt;


        }
    }

    public class NhanVienEditViewModel : NhanVienBaseViewModel
    {
        public NhanVienEditViewModel() : base() { }
    }

    public class NhanVienViewModelValidator : AbstractValidator<NhanVienViewModel>
    {
        public NhanVienViewModelValidator()
        {
            RuleFor(register => register.HoTen).NotEmpty().WithMessage("Course name cannot be empty");
        }
    }
}
