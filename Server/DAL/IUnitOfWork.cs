﻿using SHAI.Repositories.Interfaces;

namespace SHAI
{
    public interface IUnitOfWork
    {
        //ICustomerRepository Customers { get; }
        //IProductRepository Products { get; }
        //IOrdersRepository Orders { get; }
        ICoursesRepository Courses { get; }

        int SaveChanges();
    }
}
