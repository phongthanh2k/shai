import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

import { AuthService } from './services/auth.service';
import { ConfigurationService } from './services/configuration.service';
import { DxScrollViewComponent, DxTreeViewComponent } from 'devextreme-angular';
import { ScreenService } from './services/screen.service';
import { ThemeManager } from './services/theme-manager';
import { DOCUMENT } from '@angular/common';
import { AlertCommand, AlertDialog, AlertService, DialogType, MessageSeverity } from './services/alert.service';
import { NotificationService } from './services/notification.service';
import { Router } from '@angular/router';
import { LocalStoreManager } from './services/local-store-manager.service';
import { confirm } from "devextreme/ui/dialog";
import Popup from "devextreme/ui/popup";
import { AppTitleService } from './services/app-title.service';
import { ComponentLoaderDirective } from './directives/component-loader.directive';
import { SignalRService } from './services/signalr.service';
import { AccountService } from './services/account.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default,
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
        // results: any;

  @ViewChild(DxTreeViewComponent, { static: false }) menu!: DxTreeViewComponent;
  @ViewChild(DxScrollViewComponent, { static: false }) scrollView!: DxScrollViewComponent;
  @ViewChild('componentOutlet', { read: ComponentLoaderDirective, static: true }) componentOutlet!: ComponentLoaderDirective;

  notify: { visible: boolean, type: string, message?: any } = { type: 'info', visible: false };
  stickyToasties: number[] = [];

  toggleOpenMode: string = 'shrink';
  toggleRevealMode: string = 'slide';
  toggleShaderEnabled = true;
  toggleMinMenuSize = 60;
  isDrawerOpen = true;
  isXSmall = false;
  isLarge = true;

  isUserLoggedIn!: boolean;

  notificationsLoadingSubscription: any;
  dataLoadingConsecutiveFailures: any;
  newNotificationCount: number = 0;

  toggleButtonOptions = {
    icon: 'menu',
    onClick: () => this.isDrawerOpen = !this.isDrawerOpen
  };

  userMenuOptions = [
    { value: 1, name: 'Profile', icon: 'user' },
    { value: 4, name: 'Messages', icon: 'email', badge: '5' },
    { value: 2, name: 'Friends', icon: 'group' },
    { value: 3, name: 'Exit', icon: 'runner' }
  ]

  navigation = [
    {
      text: 'Home',
      path: '/',
      icon: 'home'
    },
    {
      text: 'Orders',
      icon: 'folder',
      path: '/class/orders',
      visible: this.isGrant('orders.view'),
    },
    {
      text: 'Users',
      icon: 'user',
      visible: this.isGrant('users.view') || this.isGrant('users.manage'),
      items: [
        {
          text: 'Users List',
          path: '/users',
          visible: this.isGrant('users.view'),
        },
        {
          text: 'Roles Management',
          path: '/users/roles',
          visible: this.isGrant('roles.manage'),
        }
      ]
    },
    {
      text: 'Membership',
      icon: 'card',
      visible: this.isGrant('members.view') || this.isGrant('members.manage'),
      items: [
        {
          text: 'Members List',
          path: '/users/members',
          visible: this.isGrant('members.view'),
        },
        {
          text: 'Groups Management',
          path: '/users/group',
        }
      ]
    },
    {
      text: 'Courses',
      icon: 'columnchooser',
      visible: this.isGrant('courses.view') || this.isGrant('courses.manage'),
      items: [
        {
          text: 'Courses List',
          path: '/courses',
          visible: this.isGrant('courses.view'),
        },
        {
          text: 'Courses Type Management',
          path: '/courses/genres',
        },
        {
          text: 'Class Management',
          path: '/class',
        },
        // {
        //   text: 'Roles Management',
        //   path: '/users/roles',
        //   visible: this.isGrant('roles.manage'),
        // }
      ]
    },
    {
        text: 'Nhan Vien',
        icon: 'columnchooser',
        //visible: this.isGrant('courses.view') || this.isGrant('courses.manage'),
        items: [
          {
            text: 'Danh sách Nhân viên',
            path: '/nhanvien',
            //visible: this.isGrant('courses.view'),
          }
        ]
      }
  ];

  menuItems: any = [];

  get pageTitle(): string {
    return this.appTitle.pageTitle ? ' <span class="text-muted">- ' + this.appTitle.pageTitle + '</span>' : '';
  }

  get fullName(): string {
    return this.authService.currentUser ? String(this.authService.currentUser.fullName) : '';
  }

  constructor(
    storageManager: LocalStoreManager,
    private accountService: AccountService,
    private alertService: AlertService,
    private appTitle: AppTitleService,
    private config: ConfigurationService,
    private notificationService: NotificationService,
    private router: Router,
    private screen: ScreenService,
    private signalRService: SignalRService,
    private ref: ChangeDetectorRef,
    public authService: AuthService,
    public theme: ThemeManager,
    //private http: HttpClient,
    @Inject(DOCUMENT) document: Document
  ) {

    storageManager.initialiseStorageSyncListener();

  }

  ngOnInit(): void {
        // this.http.get("http://localhost:5000/api/NhanVien/danhsach").subscribe(data => {
        //         console.log(data);
        //        this.results = data;
        //    });

    this.isUserLoggedIn = this.authService.isLoggedIn;

    // setTimeout(() => this.isAppLoaded = true, 500);
    // setTimeout(() => this.removePrebootScreen = true, 10000);

    this.alertService.getDialogEvent().subscribe(alert => this.showDialog(alert));
    this.alertService.getMessageEvent().subscribe(message => this.showToast(message));
    this.screen.changed.subscribe(() => this.updateDrawer());

    this.theme.installTheme(this.theme.getDefaultTheme())
    this.menuItems = this.navigation.map((item) => {
      if (item.path && !(/^\//.test(item.path))) {
        item.path = `/${item.path}`;
      }
      return { ...item, expanded: false }
    });

    if (this.isUserLoggedIn) {
      this.alertService.resetStickyMessage();

      if (!this.authService.isSessionExpired) {
        this.alertService.showMessage(`Welcome back ${this.fullName}!`, 'Login', MessageSeverity.default);
      }
      else {
        this.authService.refreshLogin();
      }
    }

    this.authService.getLoginStatusEvent().subscribe(isLoggedIn => {
      this.isUserLoggedIn = isLoggedIn;
      if (this.isUserLoggedIn) {
        // this.accountService.getUserPreferences().subscribe(resp => console.log(resp))

        this.initNotificationsLoading();
      } else {
        this.unsubscribeNotifications();
      }

      if (!this.isUserLoggedIn) {
        this.alertService.showMessage('Session Ended!', '', MessageSeverity.default);
      }
    });

    this.updateDrawer();

    this.authService.reLoginDelegate = () => this.authService.redirectForLogin();

    this.signalRService.onNotify().subscribe((message) => { console.log(message); });
    this.signalRService.onUpdateRole().subscribe((role) => {
      this.ref.detectChanges();
      console.log(role);
      console.log(this.accountService.permissions);
      console.log(this.isGrant('users.view'));
    });
  }

  changeTheme(t: { itemData: any }) {
    let c = JSON.parse(JSON.stringify(document.getElementsByTagName('body')[0].classList)) as any;
    let currentClass = Object.keys(c).map((key) => c[key]).pop();
    document.getElementsByTagName('body')[0].classList.remove(currentClass);
    document.getElementsByTagName('body')[0].classList.add(t.itemData.bodyClass);
    this.theme.installTheme(t.itemData);
  }

  onUserMenuTask(e: any) {
    switch (e.itemData.value) {
      case 3:
        this.authService.logout();
        this.authService.redirectLogoutUser();
        break;
    }
  }

  onMenuClick(e: any) {

    this.menu.instance.selectItem(e.itemData);
    if (e.itemData.items != null && this.isDrawerOpen == false) {
      this.isDrawerOpen = true;
    } else if (e.itemData.items == null) {

      this.router.navigate([e.itemData.path]);
      this.scrollView.instance.scrollTo(0);
      if (this.isDrawerOpen == true && this.isXSmall) {
        this.isDrawerOpen = false;
        this.menu.instance.collapseAll();
      }
    }
  }

  updateDrawer() {
    this.isXSmall = this.screen.sizes['screen-x-small'];
    this.isLarge = this.screen.sizes['screen-large'];

    this.toggleOpenMode = this.isLarge ? 'shrink' : 'overlap';
    this.toggleRevealMode = this.isXSmall ? 'slide' : 'expand';
    this.toggleMinMenuSize = this.isXSmall ? 0 : 60;
    this.toggleShaderEnabled = !this.isLarge;
  }

  showDialog(dialog: AlertDialog) {
    const self = this;
    switch (dialog.type) {
      case DialogType.alert:
        // alert(dialog.message, this.appTitle.appName);
        self.componentOutlet.embedComponent('login');
        var x = new Popup('.cus-dialog' as any, {
          title: "Popup Title",
          visible: true,
          onHidden: async () => {
            self.componentOutlet.removeComponent();
            x.dispose();
          }
        });
        x.show();
        // import('./shared/components/login-form/login-form.component').then(({ LoginFormComponent }) => {

        //   // this.componentOutlet.clear();
        //   // const factory = this.componentFactoryResolver.resolveComponentFactory(LoginFormComponent);
        //   // const ref = this.componentOutlet.createComponent(factory);
        //   // // ref.changeDetectorRef.detectChanges()
        //   // // cmpref.instance.data="Hello World";
        //   // this.renderer.appendChild(this.elementRef.nativeElement, ref.location.nativeElement);
        //   // console.log(this.componentOutlet);


        // });

        break;
      case DialogType.confirm:
        confirm(dialog.message, this.appTitle.appName).then(async isSubmit => {
          if (isSubmit) {
            dialog.okCallback();
          } else {
            if (dialog.cancelCallback) {
              dialog.cancelCallback();
            }
          }
        });
        break;
      case DialogType.prompt:
        prompt(dialog.message, '');
        break;
    }
  }

  showToast(alert: AlertCommand | any) {
    if (alert.operation === 'clear') {
      for (const id of this.stickyToasties.slice(0)) {
        this.notify.visible = false;
      }

      return;
    }


    this.notify.message = alert.message;

    switch (alert.message.severity) {
      case MessageSeverity.default: this.notify.type = 'info'; break;
      case MessageSeverity.info: this.notify.type = 'info'; break;
      case MessageSeverity.success: this.notify.type = 'success'; break;
      case MessageSeverity.error: this.notify.type = 'error'; break;
      case MessageSeverity.warn: this.notify.type = 'error'; break;
      case MessageSeverity.wait: this.notify.type = 'info'; break;
    }

    setTimeout(() => this.notify.visible = true, 100);
  }

  private initNotificationsLoading() {
    this.notificationsLoadingSubscription = this.notificationService.getNewNotificationsPeriodically()
      .subscribe(notifications => {
        this.dataLoadingConsecutiveFailures = 0;
        this.newNotificationCount = notifications.filter(n => !n.isRead).length;
      }, error => {
        this.alertService.logError(error);

        if (this.dataLoadingConsecutiveFailures++ < 20) {
          setTimeout(() => this.initNotificationsLoading(), 5000);
        } else {
          this.alertService.showStickyMessage('Load Error', 'Loading new notifications from the server failed!', MessageSeverity.error);
        }
      });
  }

  private unsubscribeNotifications() {
    if (this.notificationsLoadingSubscription) {
      this.notificationsLoadingSubscription.unsubscribe();
    }
  }

  private isGrant(permission: string): boolean {
    return this.accountService.userHasPermission(permission);
  }

  isAuthenticated() {
    return this.authService.isLoggedIn;
  }
}
