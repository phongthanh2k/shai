﻿using AutoMapper;
using SHAI.Core.Interfaces;
using SHAI.Models;

using IdentityServer4.AccessTokenValidation;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SHAI.Helpers;
using SHAI.ViewModels;

using System;
using System.Linq;
using System.Threading.Tasks;

using DevExtreme.AspNet.Data;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;

namespace SHAI.Controllers
{
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class GenreController : ControllerBase
    {
        private IHubContext<ServerHub> _hub;
        private readonly IMapper _mapper;
        private readonly IAccountManager _accountManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAuthorizationService _authorizationService;
        private readonly ILogger<AccountController> _logger;

        private readonly ApplicationDbContext _context;

        public GenreController(ApplicationDbContext context,
           IMapper mapper,
           IAccountManager accountManager,
           UserManager<ApplicationUser> userManager,
           IAuthorizationService authorizationService,
           ILogger<AccountController> logger,
           IHubContext<ServerHub> hub
           )
        {
            _hub = hub;
            _mapper = mapper;
            _accountManager = accountManager;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _logger = logger;
            _context = context;
        }

        //[HttpGet]
        //public IActionResult Get()
        //{
        //    return Ok(new { Data = _context.Genres.OrderBy("Name").Select(o => new GenreViewModel(o, _context)) });
        //}

        [HttpGet]
        public IActionResult Get(DataSourceLoadOptions loadOptions)
        {
            return Ok(DataSourceLoader.Load(_context.Genres.Select(o => new GenreViewModel(o, _context)).ToList(), loadOptions));
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(GenreViewModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> Post([FromBody] GenreEditViewModel obj)
        {
            //if (!(await _authorizationService.AuthorizeAsync(this.User, (user.Roles, new string[] { }), Authorization.Policies.AssignAllowedRolesPolicy)).Succeeded)
            //    return new ChallengeResult();

            if (ModelState.IsValid)
            {
                if (obj == null) return BadRequest($"Genre cannot be null");

                var num = _context.ScalarIntValue.FromSqlRaw(@"SELECT ISNULL(CAST((SELECT TOP 1 * FROM STRING_SPLIT((SELECT TOP 1 Id FROM dbo.Genres c ORDER BY Id DESC) , '-')  ORDER BY value ASC) AS INTEGER), 0) AS Value").FirstOrDefault().Value;

                obj.Id = Utilities.PaddInt(num + 1, 4, "SHAI-GENRE-");

                _context.Genres.Add(new Genre() { Id = obj.Id, Name = obj.Name, Description = obj.Description, Weight = obj.Weight });

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Update(string id, [FromBody] GenreEditViewModel GenreEdit)
        {
            if (ModelState.IsValid)
            {
                if (GenreEdit == null) return BadRequest($"{nameof(GenreEdit)} cannot be null");

                var Genre = await _context.Genres.FindAsync(id);
                if (Genre == null) return NotFound(id);

                Genre.Name = GenreEdit.Name;
                Genre.Description = GenreEdit.Description;
                Genre.Weight = GenreEdit.Weight;
                Genre.UpdatedDate = DateTime.Now;

                _context.Genres.Update(Genre);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        } 

        [HttpDelete("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(string id)
        {
            Genre Genre = await _context.Genres.FindAsync(id);

            if (Genre == null) return NotFound(id);

            _context.Genres.Remove(Genre);

            await _context.SaveChangesAsync();

            return NoContent();
        } 

    }
}
