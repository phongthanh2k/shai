import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { custom } from 'devextreme/ui/dialog';

import { NhanVien } from 'src/app/models/nhanvien.model';
import { NhanVienEnpoint } from 'src/app/services/nhanvien-endpoint.service';
import { NhanvienService } from 'src/app/services/nhanvien.service';

@Component({
        selector: 'app-danhsach',
        templateUrl: './danhsach.component.html',
        styleUrls: ['./danhsach.component.scss']
})
export class DanhsachComponent implements OnInit {

        public keyword!: string;
        data: NhanVien[] = [];
        //nhanvien: any;
        constructor(
                public nhanvienService: NhanvienService,
                public nhanvienEndpiont: NhanVienEnpoint,
                private router: Router,
        ) { }

        ngOnInit(): void {
                this.getAll()
        }
        //Lấy danh sách
        getAll() {
                this.nhanvienService.getAllNhanVien().subscribe((res: any) => {
                        this.data = res
                })
                console.log(this.data)
        }


        // Search() {
        //         this.nhanvienService.Search(this.keyword).subscribe((res: any) => {
        //                 this.data = res;
        //                 console.log(res);
        //         }, error => {
        //                 console.log(error);
        //         });
        // }
        // -Xóa bản ghi
        //   public deleteNhanVien(id: any){
        //           this.nhanvienService.deleteNhanVien(id).subscribe((data)=>{
        //                   console.log('delete', data);
        //                   this.getAll();
        //           })
        //   }

        async onRowDelete(e: any) {
                custom({
                        messageHtml: 'Bạn có chắc chắn xóa không ?',
                        title: 'Xác nhận',
                        buttons: [
                                { text: 'Xóa!', stylingMode: 'contained', type: 'default', onClick: (e) => true },
                                { text: 'Cancel', type: 'normal', onClick: (e) => false }
                        ]
                }).show().then(async (isSubmit: boolean) => {
                        if (isSubmit) {
                                this.nhanvienService.deleteNhanVien(e).subscribe((data) => {
                                        console.log('delete', data);
                                        this.getAll();
                                        alert("Xóa thành công!!!!")
                                });
                                // await this.dataSource.remove(e.key);

                        }
                });
        }
        public addNhanVien() {
                this.router.navigate(['nhanvien/create']);
        }

        public editNhanVien(id: any) {
                this.router.navigate(['nhanvien/create', id]);
        }
}

