
import { NgModule } from '@angular/core';
import { NhanVienRoutingModule } from './nhanvien-routing.module';
import { DanhsachComponent } from './danhsach/danhsach.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
@NgModule({
        declarations: [
        DanhsachComponent,
        CreateComponent,
        EditComponent
  ],
        imports: [
          NhanVienRoutingModule,
          ReactiveFormsModule,
          CommonModule
        ]
      })
export class NhanVienModule { }
