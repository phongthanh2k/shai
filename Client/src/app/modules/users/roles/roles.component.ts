import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxFormComponent, DxMultiViewComponent, DxSwitchComponent } from 'devextreme-angular';
import { clone, isNil } from 'lodash';

import { AbstractListComponent } from 'src/app/shared/components/abstract.component';
import { AccountService } from 'src/app/services/account.service';
import { GroupByPipe } from 'src/app/pipes/group-by.pipe';
import { Injector } from '@angular/core';
import { Role } from 'src/app/models/role.model';
import { custom } from 'devextreme/ui/dialog';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss'],
  animations: [fadeInOut],
})
export class RolesComponent extends AbstractListComponent implements OnInit {

  @ViewChild(DxDataGridComponent, { static: false }) dataSourceGrid!: DxDataGridComponent;
  @ViewChild(DxFormComponent, { static: false }) taskForm!: DxFormComponent;
  @ViewChild(DxMultiViewComponent, { static: false }) multiView!: DxMultiViewComponent;
  @ViewChild(DxSwitchComponent, { static: false }) switch!: DxSwitchComponent;

  allPermissions: any = [];
  permissionMode = 1;
  editingRoleName = '';
  public selectedValues: { [key: string]: boolean; } = {};
 
  checkBoxOptions = {
    text: "Show Address",
    value: true,
    onValueChanged: (e: any) => {
      // this.isHomeAddressVisible = e.component.option("value");
    }
  };

  constructor(injector: Injector) {
    super(injector);

    this.ayncValidate = this.ayncValidate.bind(this);
  }

  ngOnInit(): void {
    this.accountService.getRolesAndPermissions()
      .subscribe(results => {
        this.dataSource = results[0];
        this.allPermissions = results[1];
        this.selectNone();
      });
  }

  async onRowEdit(cell: any = null) {

    this.obj = (cell && cell.row && cell.row.data) ? clone(cell.row.data) : new Role();
    // this.selectedValues = (cell && cell.row && cell.row.data && cell.row.data.permissions && cell.row.data.permissions.length) ?
    //   reduce(cell.row.data.permissions, (result: any, r: any) => { result[r.value] = true; return result; }, {}) :
    //   {};
    if ((cell && cell.row && cell.row.data && cell.row.data.permissions && cell.row.data.permissions.length)) cell.row.data.permissions.forEach((p: { value: string | number; }) => this.selectedValues[p.value] = true);
    this.popupVisible = !this.popupVisible;
    // this.accountService.getRolesAndPermissions(this.obj.id).subscribe(resp => console.log(resp));
  }

  async onSave(e: any) {
    e.preventDefault();

    this.obj.permissions = this.getSelectedPermissions();
    if (isNil(this.obj.id)) {
      this.accountService.newRole(this.obj).subscribe(response => this.saveSuccessHelper(this.obj), error => this.saveFailedHelper(error));
    } else {
      this.editingRoleName = this.obj.name;
      this.accountService.updateRole(this.obj).subscribe(response => this.saveSuccessHelper(this.obj), error => this.saveFailedHelper(error));
    }

  }

  async onRowDelete(e: any) {
    custom({
      messageHtml: 'Are you sure you want to delete this record ?',
      title: 'Confirm',
      buttons: [
        { text: 'Yes, delete it!', stylingMode: 'contained', type: 'default', onClick: (e) => true },
        { text: 'Cancel', type: 'normal', onClick: (e) => false }
      ]
    }).show().then(async (isSubmit: boolean) => {
      if (isSubmit) {
        console.log(e.id);
        this.accountService.deleteRole(e.id).subscribe(resp => this.ngOnInit());
        // await e.component.refresh();
      }
    });
  }

  async onSelectPermission(e: any, v: any = null) {
    this.selectedValues[v] = e;
  }

  async selectionChanged(e: any) {
    e.component.collapseAll(-1);
    e.component.expandRow(e.currentSelectedRowKeys[0]);
  }

  async onHidePopup() {
    this.selectNone();
    this.obj = new Role();
    this.selectedRows = [];
    if (this.taskForm) this.taskForm.instance.resetValues();
  }

  async selectAll() {
    this.allPermissions.forEach((p: { value: string | number; }) => this.selectedValues[p.value] = true);
  }

  async selectNone() {
    this.allPermissions.forEach((p: { value: string | number; }) => this.selectedValues[p.value] = false);
  }

  getViewIndex(i: any = 0): string {
    var g = new GroupByPipe();

    return `<div class="text-muted">Item <b class="text-highlight">${(this.multiView?.selectedIndex ?? 0) + 1}</b> of <b>${g.transform(this.allPermissions, 'groupName').length}</b> <i>(Swipe the view horizontally to switch to the next view.)</i></div>`;

  }

  private saveSuccessHelper(role?: Role) {

    this.popupVisible = false;
    this.ngOnInit();

    if (this.changesSavedCallback) {
      this.changesSavedCallback();
    }
  }

  private getSelectedPermissions() {
    return this.allPermissions.filter((p: { value: string | number; }) => this.selectedValues[p.value] === true);
  }
}
