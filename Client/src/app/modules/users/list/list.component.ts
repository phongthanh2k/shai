import { Component, OnInit, ViewChild } from '@angular/core';
import { DxDataGridComponent, DxFormComponent } from 'devextreme-angular';

import { AbstractListComponent } from 'src/app/shared/components/abstract.component';
import CustomStore from 'devextreme/data/custom_store';
import { HttpParams } from '@angular/common/http';
import { Injector } from '@angular/core';
import { Role } from 'src/app/models/role.model';
import { UserEdit } from 'src/app/models/user-edit.model';
import { clone } from 'lodash';
import { custom } from 'devextreme/ui/dialog';
import { fadeInOut } from 'src/app/services/animations';

@Component({
  selector: 'app-user-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  animations: [fadeInOut],
})
export class ListComponent extends AbstractListComponent implements OnInit {

  @ViewChild(DxDataGridComponent, { static: false }) dataSourceGrid!: DxDataGridComponent;
  @ViewChild(DxFormComponent, { static: false }) taskForm!: DxFormComponent;

  accountStatus = [
    { value: true, display: 'Active' },
    { value: false, display: 'Locked' }
  ];

  genders = [
    'Male',
    'Female',
    'Unknown'
  ];

  roles: Role[] = [];
  rolesFilter: any = [];
  roleDataSource: any;
  rolesValue: any[] = [];

  isClearFilter = true;

  constructor(injector: Injector) {
    super(injector);

    this.ayncValidate = this.ayncValidate.bind(this);
    this.onCustomRule = this.onCustomRule.bind(this);
  }

  ngOnInit(): void {
    const self = this;

    this.dataSource = new CustomStore({
      key: 'id',
      load: async (loadOptions: any) => {
        let params: HttpParams = new HttpParams();

        if (loadOptions.filter) {
          loadOptions.filter = [loadOptions.filter];
          loadOptions.filter.push('and');
          loadOptions.filter.push(['isStaff', '=', true]);
        } else {
          loadOptions.filter = ['isStaff', '=', true];
        }

        this.queryParams.forEach((i) => {
          if (i in loadOptions && this.isNotEmpty(loadOptions[i])) params = params.set(i, JSON.stringify(loadOptions[i]));
        });

        return this.accountService.getAllUsers(params).toPromise();
      }
    });

    this.roleDataSource = new CustomStore({
      loadMode: "raw",
      key: "name",
      load: async () => {
        return self.accountService.getRoles().toPromise();
      }
    });

    self.accountService.getRoles().subscribe(resp => {
      self.rolesFilter = [];
      resp.map(e => self.rolesFilter.push({ text: e.name, value: ['roleString', 'contains', e.name] }));
    });
  }

  async onToolbarPreparing(e: any) {
    const self = this;
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        widget: 'dxButton',
        options: {
          template: 'deleteButton',
          onClick: this.onRowEdit = this.onRowEdit.bind(this)
        }
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'refresh',
          stylingMode: 'contained',
          onClick: this.onClearFiler = this.onClearFiler.bind(this)
        }
      },
      {
        location: 'after',
        widget: 'dxButton',
        options: {
          icon: 'add',
          type: 'default', stylingMode: 'contained',
          onClick: this.onRowEdit = this.onRowEdit.bind(this)
        }
      }
    );
  }

  async onClearFiler() {
    this.dataSourceGrid.instance.clearFilter();
    this.isClearFilter = true;
  }

  async onSave(e: any) {
    e.preventDefault();
    this.obj.roles = this.rolesValue;
    if (this.obj.id) {
      this.accountService.updateUser(this.obj).subscribe(resp => {
        this.popupVisible = false;
        this.dataSourceGrid.instance.refresh();
      }, error => console.log(error));
    } else {
      this.obj.newPassword = this.obj.confirmPassword = this.obj.currentPassword;
      this.obj.configuration = '';
      this.obj.isStaff = true;
      this.accountService.newUser(this.obj).subscribe(resp => {
        this.popupVisible = false;
        this.dataSourceGrid.instance.refresh();
      }, error => console.log(error));
    }
  }

  async onRowEdit(cell: any = null) {
    this.obj = (cell && cell.row && cell.row.data) ? clone(cell.row.data) : new UserEdit();
    this.rolesValue = (cell && cell.row && cell.row.data) ? cell.row.data.roles : [];
    this.popupVisible = !this.popupVisible;
  }

  async onRowDelete(e: any) {
    custom({
      messageHtml: 'Are you sure you want to delete this record ?',
      title: 'Confirm',
      buttons: [
        { text: 'Yes, delete it!', stylingMode: 'contained', type: 'default', onClick: (e) => true },
        { text: 'Cancel', type: 'normal', onClick: (e) => false }
      ]
    }).show().then(async (isSubmit: boolean) => {
      if (isSubmit) {
        this.accountService.deleteUser(e.key).subscribe(resp => {
          e.component.refresh();
        });
        // await this.dataSource.remove(e.key);

      }
    });
  }

  async onCustomRule() {
    return false;
  }

  valueChanged(e: any) {
    // this.emailValue = data.value.replace(/\s/g, '').toLowerCase() + '@corp.com';
    console.log(e.component, e.component._changedValue, this.obj);
  }
}
