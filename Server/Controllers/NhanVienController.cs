﻿using AutoMapper;
using AutoMapper.Configuration;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SHAI.Core.Interfaces;
using SHAI.DAL.Models;
using SHAI.Helpers;
using SHAI.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Net.Http;
using System;
using System.Net.Http.Formatting;
using System.Net;
using System.Linq.Dynamic.Core;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SHAI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NhanVienController : ControllerBase
    {
        //private IHubContext<ServerHub> _hub;
        private readonly IMapper _mapper;
        private readonly IAccountManager _accountManager;
        //private readonly UserManager<ApplicationUser> _userManager;
        //private readonly IAuthorizationService _authorizationService;
        //private readonly ILogger<AccountController> _logger;
        private const string GetNhanVienByIdActionName = "GetNhanVienById";

        private readonly ApplicationDbContext _context;

        public NhanVienController(ApplicationDbContext context,
           IMapper mapper,
           IAccountManager accountManager
           //UserManager<ApplicationUser> userManager,
           //IAuthorizationService authorizationService,
           //ILogger<AccountController> logger,
           //IHubContext<ServerHub> hub
           )
        {
            //_hub = hub;
            _mapper = mapper;
            _accountManager = accountManager;
            //_userManager = userManager;
            //_authorizationService = authorizationService;
            //_logger = logger;
            _context = context;
        }
        [HttpGet("nhanvien")]
        [ProducesResponseType(200, Type = typeof(List<NhanVienViewModel>))]
        public IActionResult GetNhanVien(DataSourceLoadOptions loadOptions)
        {
            var nhanvien = _context.NhanViens.Select(o => new NhanVienViewModel(o)).ToList();
            return Ok(nhanvien);
            //return Ok(DataSourceLoader.Load(_context.NhanViens.Select(o => new NhanVienViewModel(o, _context)).ToArray(), loadOptions));
        }

        // GET: api/<NhanVienController>
        //[HttpGet]
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}
        
        // GET api/<NhanVienController>/5
        [HttpGet("nhanvien/create/{id}")]
        public async Task<ActionResult<NhanVien>> GetNhanVienById(int id)
        {
            //if (!(await _authorizationService.AuthorizeAsync(this.User, id, AccountManagementOperations.Read)).Succeeded)
            //    return new ChallengeResult();

            var nhanvien = await _context.NhanViens.FindAsync(id);
            if(nhanvien == null)
            {
                return NotFound();
            }
            return nhanvien;
            //NhanVienViewModel nhanvienVM = await GetNhanVienViewModelHelper(id);

            //if (nhanvienVM != null)
            //    return Ok(nhanvienVM);
            //else
            //    return NotFound(id);
        }

        // POST api/<NhanVienController>
        [HttpPost("nhanvien/create")]
        [ProducesResponseType(201, Type = typeof(NhanVienViewModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> Post([FromBody] NhanVienViewModel obj)
        {

            if (ModelState.IsValid)
            {
                if (obj == null) return BadRequest($"{nameof(NhanVien)} cannot be null");

                //var num = _context.ScalarIntValue.FromSqlRaw(@"SELECT ISNULL(CAST((SELECT TOP 1 * FROM STRING_SPLIT((SELECT TOP 1 Id FROM dbo.NhanVien c ORDER BY Id DESC) , '-')  ORDER BY value ASC) AS INTEGER), 0) AS Value").FirstOrDefault().Value;

                //obj.Id = Utilities.PaddInt(num + 1, 4, "SHAI-HY-");
                var nhanvien = _context.NhanViens.Select(o=> new NhanVienViewModel(o)).ToArray();
                _context.NhanViens.Add(new NhanVien()
                {
                    Id = obj.Id,
                    HoTen = obj.HoTen,
                    GioiTinh = obj.GioiTinh,
                    SoThich = obj.SoThich,
                    HocLuc = obj.HocLuc,
                    Sdt = obj.Sdt
                });

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }

        }


        //PUT api/<NhanVienController>/5
        [HttpPut("nhanvien/create/{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Update(int id, [FromBody] NhanVienViewModel obj)
        {
            if (ModelState.IsValid)
            {
                if (obj == null) return BadRequest($"Course cannot be null");

                var nhanvien = await _context.NhanViens.FindAsync(id);
                if (nhanvien == null) return NotFound(id);

                nhanvien.HoTen = obj.HoTen;
                nhanvien.GioiTinh = obj.GioiTinh;
                nhanvien.SoThich = obj.SoThich;
                nhanvien.HocLuc = obj.HocLuc;
                nhanvien.Sdt = obj.Sdt;

                _context.NhanViens.Update(nhanvien);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        // DELETE api/<NhanVienController>/5
        [HttpDelete("nhanvien/{id}")]
        //[ProducesResponseType(400)]
        //[ProducesResponseType(403)]
        //[ProducesResponseType(404)]
        public async Task<IActionResult> Delete(int id)
        {
            NhanVien nhanVien = await _context.NhanViens.FindAsync(id);

            if (nhanVien == null) return NotFound(id);

            _context.NhanViens.Remove(nhanVien);

            await _context.SaveChangesAsync();

            return NoContent();
        }

        //private async Task<NhanVienViewModel> GetNhanVienViewModelHelper(int id)
        //{
        //    var nhanvienAndRoles = await _accountManager.GetUserAndRolesAsync(id);
        //    if (nhanvienAndRoles == null)
        //        return null;

        //    var nhanvienVM = _mapper.Map<NhanVienViewModel>(nhanvienAndRoles.Value.User);
        //    //nhanvienVM.Roles = nhanvienAndRoles.Value.Roles;

        //    return nhanvienVM;
        //}
    }
}
