// @ts-nocheck

import { AlertService } from './alert.service';
import { AuthService } from './auth.service';
import { ConfigurationService } from './configuration.service';
import { EndpointBase } from './endpoint-base.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class NhanVienEnpoint extends EndpointBase {

        const apiUrl = 'http://localhost:5000/api/NhanVien/nhanvien';
        //const apiURL = 'http://localhost:5000/api/NhanVien';
        get currentNhanVienUrl() { return this.configurations.baseUrl + '/api/NhanVien/'; }

  constructor(private configurations: ConfigurationService, http: HttpClient, authService: AuthService, alertService: AlertService) {
    super(http, authService, alertService);
  }
  getNhanVienAllEndpoint<T>(nhanvien: NhanVien): Observable<T> {
        const endpointUrl = this.apiUrl;
        var { headers } = this.requestHeaders;
        return this.http.get<T>(endpointUrl, { headers, nhanvien: nhanvien }).pipe<T>(
          catchError(error => {
            return this.handleError(error, () => this.getNhanVienAllEndpoint(nhanvien));
          }));
      }

        getNewNhanVienEndpoint(nhanvien: NhanVien) {
        const url = `${this.apiUrl}/create`;
        return this.http.post(url, JSON.stringify(nhanvien), this.requestHeaders).pipe<T>(

          catchError(error => {
            return this.handleError(error, () => this.getNewNhanVienEndpoint(nhanvien));
          }));
        }
        getUpdateNhanVienEndpoint<T>(nhanvien: any, Id?: string): Observable<T> {
                const endpointUrl = Id ? `${this.apiUrl}/${Id}` : this.currentNhanVienUrl;

                return this.http.put<T>(endpointUrl, JSON.stringify(nhanvien), this.requestHeaders).pipe<T>(

                  catchError(error => {
                    return this.handleError(error, () => this.getUpdateNhanVienEndpoint(nhanvien, Id));
                  }));
              }

        getDeleteNhanVienEndpoint<T>(id: string): Observable<T> {
                const endpointUrl = `${this.apiUrl}/${id}`;

                return this.http.delete<T>(endpointUrl, this.requestHeaders).pipe<T>(

                  catchError(error => {
                    return this.handleError(error, () => this.getDeleteNhanVienEndpoint(id));
                  }));
              }

        delete(id: string){
                console.log("ID cua ban " +id);
                const endpointUrl = `${this.apiUrl}/${id}`;
                return this.http.delete(endpointUrl, this.requestHeaders).pipe(map(data => {}));
        }

}
