import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NhanVien } from 'src/app/models/nhanvien.model';
import { NhanVienEnpoint } from 'src/app/services/nhanvien-endpoint.service';
import { NhanvienService } from 'src/app/services/nhanvien.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
public id = 0 ;
public nhanvienForm = new FormGroup({
                id: new FormControl(''),
                hoTen: new FormControl(''),
                gioiTinh: new FormControl(''),
                soThich: new FormControl(''),
                hocLuc: new FormControl(''),
                sdt: new FormControl(''),
        });

  constructor(
        public nhanvienEnpoint: NhanVienEnpoint,
        public nhanvienService: NhanvienService,
        private router: Router,
        private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
        this.id = +this.route.snapshot.paramMap.has('id');
        if(this.id>0){
                this.loadData(this.id);
        }
  }
  private loadData(id: number) {
        console.log('Load id: ', id);
        this.nhanvienService.getNhanVien(id).subscribe((nhanvien) => {
                console.log('getNhanVien', nhanvien);
        for(const controlName in this.nhanvienForm.controls){
                if(controlName){
                        this.nhanvienForm.controls[controlName].setValue(nhanvien[controlName]);
                }
        }
})
}

private createNewData(){
        const newNhanVien:any = {};
        for(const controlName in this.nhanvienForm.controls){
                if(controlName){
                        newNhanVien[controlName] =this.nhanvienForm.controls[controlName].value;
                }
        }
        return newNhanVien as NhanVien;
}
public save(){
        this.nhanvienService.modifyNhanVien(this.id, this.createNewData()).subscribe((nhanvien)=>{
                this.router.navigate(['nhanvien']);
        })
}

}
