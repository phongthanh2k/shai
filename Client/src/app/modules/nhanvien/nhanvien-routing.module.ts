import { RouterModule, Routes } from '@angular/router';

import { NgModule } from '@angular/core';
import { DanhsachComponent } from './danhsach/danhsach.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';

const routes: Routes = [
        {
          path: '',
          component: DanhsachComponent, data: { title: 'Nhân viên' }
        },
        {
          path: 'create',
          component: CreateComponent, data: { title: 'Thêm mới nhân viên' }
        },
        {
          path: 'edit',
          component: EditComponent, data: { title: 'Thêm mới nhân viên' }
        },
      ];

      @NgModule({
        imports: [RouterModule.forChild(routes)],
        exports: [RouterModule]
      })
      export class NhanVienRoutingModule { }
