﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using SHAI.DAL.Models;
using SHAI.Models;
using SHAI.Models.Interfaces;

using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SHAI
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public string CurrentUserId { get; set; }

        public virtual DbSet<ScalarInt> ScalarIntValue { get; set; }

        public virtual DbSet<Genre> Genres { get; set; }

        public virtual DbSet<Course> Courses { get; set; }

        public virtual DbSet<ClassRoom> ClassRooms { get; set; }

        public virtual DbSet<Group> Groups { get; set; }

        public virtual DbSet<Order> Orders { get; set; }

        public virtual DbSet<NhanVien> NhanViens { get; set; }

        public ApplicationDbContext(DbContextOptions options) : base(options)
        { }

        public ApplicationDbContext()
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            //const string priceDecimalType = "decimal(18,2)";

            builder.Entity<ScalarInt>().HasNoKey();

            builder.Entity<ApplicationUser>().HasMany(u => u.Claims).WithOne().HasForeignKey(c => c.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationUser>().HasMany(u => u.Roles).WithOne().HasForeignKey(r => r.UserId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationUser>().HasIndex(u => u.Email).IsUnique();
            builder.Entity<ApplicationUser>().HasIndex(u => u.PhoneNumber).IsUnique();
            builder.Entity<ApplicationUser>().HasMany(u => u.Groups).WithMany(c => c.Users).UsingEntity(e => e.ToTable("UserGroups"));

            builder.Entity<ApplicationUser>().ToTable("Users");

            builder.Entity<ApplicationRole>().HasMany(r => r.Claims).WithOne().HasForeignKey(c => c.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationRole>().HasMany(r => r.Users).WithOne().HasForeignKey(r => r.RoleId).IsRequired().OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ApplicationRole>().ToTable("Roles");

            builder.Entity<IdentityUserRole<string>>(entity => { entity.ToTable("UserRoles"); });
            builder.Entity<IdentityUserClaim<string>>(entity => { entity.ToTable("UserClaims"); });
            builder.Entity<IdentityUserLogin<string>>(entity => { entity.ToTable("UserLogins"); });
            builder.Entity<IdentityUserToken<string>>(entity => { entity.ToTable("UserTokens"); });
            builder.Entity<IdentityRoleClaim<string>>(entity => { entity.ToTable("RoleClaims"); });

            builder.Entity<Genre>().Property(c => c.Name).IsRequired().HasMaxLength(255);
            builder.Entity<Genre>().HasIndex(c => c.Name).IsUnique();
            builder.Entity<Genre>().HasMany(o => o.Courses).WithOne(o => o.Genre).OnDelete(DeleteBehavior.SetNull);
            builder.Entity<Genre>().ToTable("Genres");

            builder.Entity<Course>().Property(c => c.Name).IsRequired().HasMaxLength(512);
            builder.Entity<Course>().HasIndex(c => c.Name);
            builder.Entity<Course>().ToTable("Courses");

            builder.Entity<ClassRoom>().HasIndex(c => c.Name);
            //builder.Entity<ClassRoom>().HasOne(o => o.Course).WithOne(o => o.ClassRoom).HasForeignKey<Course>(o => o.Id).OnDelete(DeleteBehavior.SetNull);
            builder.Entity<ClassRoom>().HasMany(u => u.Users).WithMany(c => c.ClassRooms).UsingEntity(e => e.ToTable("UserClassRooms"));
            builder.Entity<ClassRoom>().HasOne(a => a.Course).WithMany(b => b.ClassRooms);
            builder.Entity<ClassRoom>().ToTable("ClassRooms");

            builder.Entity<Order>().ToTable("Orders");

            builder.Entity<NhanVien>().Property(c => c.HoTen).IsRequired().HasMaxLength(512);
            builder.Entity<NhanVien>().HasIndex(c => c.HoTen);
            builder.Entity<NhanVien>().ToTable("NhanVien");
        }

        public override int SaveChanges()
        {
            UpdateAuditEntities();
            return base.SaveChanges();
        }


        public override int SaveChanges(bool acceptAllChangesOnSuccess)
        {
            UpdateAuditEntities();
            return base.SaveChanges(acceptAllChangesOnSuccess);
        }


        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(cancellationToken);
        }


        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken))
        {
            UpdateAuditEntities();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }


        private void UpdateAuditEntities()
        {
            var modifiedEntries = ChangeTracker.Entries()
                .Where(x => x.Entity is IAuditableEntity && (x.State == EntityState.Added || x.State == EntityState.Modified));


            foreach (var entry in modifiedEntries)
            {
                var entity = (IAuditableEntity)entry.Entity;
                DateTime now = DateTime.UtcNow;

                if (entry.State == EntityState.Added)
                {
                    entity.CreatedDate = now;
                    entity.CreatedBy = CurrentUserId;
                }
                else
                {
                    base.Entry(entity).Property(x => x.CreatedBy).IsModified = false;
                    base.Entry(entity).Property(x => x.CreatedDate).IsModified = false;
                }

                entity.UpdatedDate = now;
                entity.UpdatedBy = CurrentUserId;
            }
        }
    }
}
