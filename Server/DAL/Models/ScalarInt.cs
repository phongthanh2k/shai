﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SHAI.DAL.Models
{
    public partial class ScalarInt
    {
        public int Value { get; set; }

        public override string ToString()
        {
            return Value.ToString();
        }

        public bool ToBool()
        {
            return Value > 0;
        }
    }
}
