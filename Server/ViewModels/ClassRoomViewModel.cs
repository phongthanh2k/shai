﻿using FluentValidation;

using Microsoft.EntityFrameworkCore;

using SHAI.Models;

using System;
using System.Collections.Generic;
using System.Linq;

namespace SHAI.ViewModels
{
    public class ClassRoomBaseViewModel : AuditableEntity
    {
        public string Id { get; set; }
        public string CourseId { get; set; }

        public string Name { get; set; }
        public string Lecturers { get; set; }
        public string Code { get; set; }
        public string Venue { get; set; }
        public string Description { get; set; }

        public int Fee { get; set; }

        public int Status { get; set; }

        public DateTime ScheduleDate { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public virtual ICollection<ApplicationUser> Users { get; set; }
        public virtual Course Course { get; set; }
    }

    public class ClassRoomViewModel : ClassRoomBaseViewModel
    {
        public ClassRoomViewModel() : base() { }

        public ClassRoomViewModel(ClassRoom o)
        {
            Id = o.Id;
            Name = o.Name;
            Lecturers = o.Lecturers;
            Venue = o.Venue;
            Fee = o.Fee;
            Description = o.Description;
            ScheduleDate = o.ScheduleDate;
            StartTime = o.StartTime;
            EndTime = o.EndTime;
            CourseId = o.CourseId;
            Status = o.Status;
            CreatedBy = o.CreatedBy;
            UpdatedBy = o.UpdatedBy;
            CreatedDate = o.CreatedDate;
            UpdatedDate = o.UpdatedDate;
        }

        public ClassRoomViewModel(ClassRoom o, ApplicationDbContext _context)
        {
            Id = o.Id;
            Name = o.Name;
            Lecturers = o.Lecturers;
            Venue = o.Venue;
            Fee = o.Fee;
            Description = o.Description;
            ScheduleDate = o.ScheduleDate;
            StartTime = o.StartTime;
            EndTime = o.EndTime;
            CourseId = o.CourseId;
            Course = _context.Courses.FromSqlRaw(@"SELECT * FROM dbo.Courses WHERE Id = {0}", o.CourseId).FirstOrDefault();
            Status  = o.Status;
            CreatedBy = o.CreatedBy;
            UpdatedBy = o.UpdatedBy;
            CreatedDate = o.CreatedDate;
            UpdatedDate = o.UpdatedDate;
        }
    }

    public class ClassRoomEditViewModel  
    {
        public ClassRoomEditViewModel() : base() { }

        public string Id { get; set; }
        public string CourseId { get; set; }

        public string Name { get; set; }
        public string Lecturers { get; set; }
        public string Code { get; set; }
        public string Venue { get; set; }
        public string Description { get; set; }

        public int Fee { get; set; }

        public int Status { get; set; }

        public DateTime ScheduleDate { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }

    public class ClassRoomViewModelValidator : AbstractValidator<ClassRoomViewModel>
    {
        public ClassRoomViewModelValidator()
        {
            RuleFor(register => register.Name).NotEmpty().WithMessage("ClassRoom name cannot be empty");
        }
    }
}
