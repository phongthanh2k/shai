﻿using AutoMapper;
using SHAI.Core.Interfaces;
using SHAI.Models;

using IdentityServer4.AccessTokenValidation;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SHAI.Helpers;
using SHAI.ViewModels;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using DevExtreme.AspNet.Data;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Identity;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;

namespace SHAI.Controllers
{
    [Authorize(AuthenticationSchemes = IdentityServerAuthenticationDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    public class ClassRoomsController : ControllerBase
    {
        private IHubContext<ServerHub> _hub;
        private readonly IMapper _mapper;
        private readonly IAccountManager _accountManager;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IAuthorizationService _authorizationService;
        private readonly ILogger<AccountController> _logger; 

        private readonly ApplicationDbContext _context;

        public ClassRoomsController(ApplicationDbContext context,
           IMapper mapper,
           IAccountManager accountManager,
           UserManager<ApplicationUser> userManager,
           IAuthorizationService authorizationService,
           ILogger<AccountController> logger,
           IHubContext<ServerHub> hub
           )
        {
            _hub = hub;
            _mapper = mapper;
            _accountManager = accountManager;
            _userManager = userManager;
            _authorizationService = authorizationService;
            _logger = logger;
            _context = context;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(List<ClassRoomViewModel>))]
        public IActionResult Get(DataSourceLoadOptions loadOptions)
        {
            return Ok(DataSourceLoader.Load(_context.ClassRooms.Select(o => new ClassRoomViewModel(o, _context)).ToArray(), loadOptions));
        }

        [HttpGet("{id}")]
        [ProducesResponseType(200, Type = typeof(ClassRoomViewModel))]
        public IActionResult GetById(string id)
        {
            return Ok(_context.ClassRooms.Where(o => o.Id == id).Select(o => new ClassRoomViewModel(o, _context)).FirstOrDefault());
        }

        [HttpPost]
        [ProducesResponseType(201, Type = typeof(ClassRoomViewModel))]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        public async Task<IActionResult> Post([FromBody] ClassRoomEditViewModel obj)
        {
            //if (!(await _authorizationService.AuthorizeAsync(this.User, (user.Roles, new string[] { }), Authorization.Policies.AssignAllowedRolesPolicy)).Succeeded)
            //    return new ChallengeResult();

            if (ModelState.IsValid)
            {
                if (obj == null) return BadRequest($"{nameof(ClassRoom)} cannot be null");

                var num = _context.ScalarIntValue.FromSqlRaw(@"SELECT ISNULL(CAST((SELECT TOP 1 * FROM STRING_SPLIT((SELECT TOP 1 Id FROM dbo.ClassRooms c ORDER BY Id DESC) , '-')  ORDER BY value ASC) AS INTEGER), 0) AS Value").FirstOrDefault().Value;

                obj.Id = Utilities.PaddInt(num + 1, 4, "SHAI-HY-");

                _context.ClassRooms.Add(new ClassRoom()
                {
                    Id = obj.Id,
                    Name = obj.Name,
                    Lecturers = obj.Lecturers,
                    Venue = obj.Venue,
                    Description = obj.Description,
                    Fee = obj.Fee,
                    ScheduleDate = obj.ScheduleDate,
                    StartTime = obj.StartTime.TimeOfDay,
                    EndTime = obj.EndTime.TimeOfDay,
                    Status = obj.Status,
                });

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }

        }

        [HttpPut("{id}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Update(string id, [FromBody] ClassRoomEditViewModel obj)
        {
            if (ModelState.IsValid)
            {

                if (obj == null) return BadRequest($"ClassRoom cannot be null");

                var o = await _context.ClassRooms.FindAsync(id);
                if (o == null) return NotFound(id);

                o.Name = obj.Name;
                o.CourseId = obj.CourseId;
                o.Lecturers = obj.Lecturers;
                o.Venue = obj.Venue;
                o.Description = obj.Description;
                o.Fee = obj.Fee;
                o.ScheduleDate = obj.ScheduleDate;
                o.StartTime = new DateTime(1970, 1, 1, obj.StartTime.TimeOfDay.Hours + 7, obj.StartTime.TimeOfDay.Minutes, 0).TimeOfDay;
                o.EndTime = new DateTime(1970, 1, 1, obj.EndTime.TimeOfDay.Hours + 7, obj.StartTime.TimeOfDay.Minutes, 0).TimeOfDay;
                o.Status = obj.Status;

                o.UpdatedDate = DateTime.Now;

                _context.ClassRooms.Update(o);

                await _context.SaveChangesAsync();

                return NoContent();
            }
            else
            {
                return BadRequest(ModelState);
            }
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(403)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> Delete(string id)
        {
            ClassRoom ClassRoom = await _context.ClassRooms.FindAsync(id);

            if (ClassRoom == null) return NotFound(id);

            _context.ClassRooms.Remove(ClassRoom);

            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
