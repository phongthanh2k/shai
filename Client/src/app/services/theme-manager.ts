import { AppTheme } from '../models/app-theme.model';
import { Injectable } from '@angular/core';

@Injectable()
export class ThemeManager {
    themes: Array<AppTheme> = [
        {
            id: 1,
            name: 'Light',
            href: 'dx.material.orange.light.css',
            isDefault: true,
            background: '#007bff',
            color: '#fff',
            bodyClass: 'dx-color-scheme-orange-light'
        },
        {
            id: 2,
            name: 'Dark',
            href: 'dx.material.orange.dark.css',
            isDefault: false,
            background: '#2780E3',
            color: '#373a3c',
            bodyClass: 'dx-color-scheme-orange-dark'
        }
    ];

    public installTheme(theme?: AppTheme) { 
        if (theme == null) {
           // this.removeStyle('theme');
        } else {
            this.setStyle('theme', `assets/css/${theme.href}`);
        }
    }

    public getDefaultTheme(): AppTheme | any {
        return this.themes.find(theme => theme.isDefault);
    }

    public getThemeByID(id: number): AppTheme | any {
        return this.themes.find(theme => theme.id === id);
    }

    private setStyle(key: string, href: string) {
        this.getLinkElementForKey(key).setAttribute('href', href);
    }

    private removeStyle(key: string) {
        const existingLinkElement = this.getExistingLinkElementByKey(key);
        if (existingLinkElement) {
            document.head.removeChild(existingLinkElement);
        }
    }

    private getLinkElementForKey(key: string) {
        return this.getExistingLinkElementByKey(key) || this.createLinkElementWithKey(key);
    }

    private getExistingLinkElementByKey(key: string) {
        return document.head.querySelector(`link[rel="stylesheet"].${this.getClassNameForKey(key)}`);
    }

    private createLinkElementWithKey(key: string) {
        const linkEl = document.createElement('link');
        linkEl.setAttribute('rel', 'stylesheet');
        linkEl.classList.add(this.getClassNameForKey(key));
        document.head.appendChild(linkEl);
        return linkEl;
    }

    private getClassNameForKey(key: string) {
        return `style-manager-${key}`;
    }
}
