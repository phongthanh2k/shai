import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NhanVien } from 'src/app/models/nhanvien.model';
import { NhanVienEnpoint } from 'src/app/services/nhanvien-endpoint.service';
import { NhanvienService } from 'src/app/services/nhanvien.service';

@Component({
        selector: 'app-create',
        templateUrl: './create.component.html',
        styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
        submit = false;
        public id = 0;
        public nhanvienForm = new FormGroup({
                // id: new FormControl(''),
                hoTen: new FormControl('', [Validators.required]),
                gioiTinh: new FormControl('Nam'),
                soThich: new FormControl('Không'),
                hocLuc: new FormControl('Giỏi'),
                sdt: new FormControl('', [Validators.required, Validators.minLength(10), Validators.maxLength(64)]),
        });
        constructor(
                public nhanvienEnpoint: NhanVienEnpoint,
                public nhanvienService: NhanvienService,
                private router: Router,
                private route: ActivatedRoute,
        ) { }

        ngOnInit(): void {
                this.id = Number(this.route.snapshot.paramMap.get('id'));
                if (this.id > 0) {
                        this.loadData(this.id);
                }
        }

        private loadData(id: any) {
                console.log('Load id: ', id);
                this.nhanvienService.getNhanVien(id).subscribe((nhanvien) => {
                        console.log('getNhanVien', nhanvien);
                        for (const controlName in this.nhanvienForm.controls) {
                                if (controlName) {
                                        this.nhanvienForm.controls[controlName].setValue(nhanvien[controlName]);
                                }
                        }
                })
        }

        private createNewData() {
                const newNhanVien: any = {};
                for (const controlName in this.nhanvienForm.controls) {
                        if (controlName) {
                                newNhanVien[controlName] = this.nhanvienForm.controls[controlName].value;
                        }
                }
                return newNhanVien as NhanVien;
        }
        public onSubmit() {
                this.submit = true;
                if (this.nhanvienForm.invalid) {
                        return;
                }
                console.log('onSubmit');
                const newNhanVien: any = {};
                for (const controlName in this.nhanvienForm.controls) {
                        if (controlName) {
                                newNhanVien[controlName] = this.nhanvienForm.controls[controlName].value;
                        }
                }
                //console.log(newNhanVien);
                this.nhanvienService.newNhanVien(newNhanVien as NhanVien).subscribe((nhanvien) => {
                        console.log('Thêm mới: ', nhanvien);
                        this.router.navigate(['nhanvien']);
                        alert('Thêm mới thành công!')
                });

                //   this.nhanvienService.create(this.form.value).subscribe(res=>{
                //           console.log('Created successfully');
                //           this.router.navigateByUrl('/nhanvien/');
                //   })
        }

        public save() {
                this.nhanvienService.modifyNhanVien(this.id, this.createNewData()).subscribe((nhanvien) => {
                        console.log('Cập nhật: ', nhanvien)
                        this.router.navigate(['nhanvien']);
                        alert('Cập nhật thành công!')
                })
        }

        get f() { return this.nhanvienForm.controls; }

        //   nhanvien_validation_messages ={
        //           'hoTen':[
        //                   {type: 'required', message: 'Mời nhập tên'},
        //           ],
        //           'sdt':[
        //                   {type: 'required', message: 'Mời nhập số điện thoại'},
        //                   {type: 'minlength', message: 'Số điện thoại phải lớn hơn 10!'},
        //                   {type: 'maxlength', message: 'Số điện thoại phải nhỏ hơn 64!!'},
        //           ]
        //   }
}
